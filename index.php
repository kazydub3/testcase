<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

$errors = [
    1 => "Task Form error: Task Name can not be empty",
    2 => "Task Form error: workers can not be empty",
    3 => "Error occured",
    4 => "PIB can not be empty",
];

$error_id = isset($_GET['error']) ? (int)$_GET['error'] : 0;
if ($error_id != 0 && array_key_exists($error_id, $errors)) {
    echo $errors[$error_id];
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <title>List of Tasks</title>
</head>
<body>
<?php
include_once("database/DataBase.php");
$tasksPerWorker = (new DataBase())->getCountPerWorker() ?? []; ?>
<?php $workerPerTask = (new DataBase())->getCountPerTask() ?? []; ?>

<!--Workers with count of tasks-->
<table class="table">
    <thead>
    <tr>
        <td>ПІБ</td>
        <td>Кількість задач закріплених за користувачем</td>
    </tr>
    </thead>
    <tbody>
    <?php
    if ( ! empty($tasksPerWorker)):
        ?>
        <?php
        foreach ($tasksPerWorker as $item): ?>
            <tr>
                <td><?php
                    echo $item[0] ?></td>
                <td><?php
                    echo $item[1] ?></td>
            </tr>
        <?php
        endforeach; ?>
    <?php
    else: ?>
        <p> Немає працівників</p>
    <?php
    endif; ?>
    </tbody>
</table>

<br>
<br>
<br>
<!--Task with count of workers-->
<table class="table">
    <thead>
    <tr>
        <td>Назва</td>
        <td>Статус</td>
        <td>Кількість користувачів закріпленіх за задаччою</td>
    </tr>
    </thead>
    <tbody>
    <?php
    if ( ! empty($workerPerTask)): ?>
        <?php
        foreach ($workerPerTask as $item): ?>
            <tr>
                <td><?php
                    echo $item[0] ?></td>
                <td><?php
                    echo $item[1] ?></td>
                <td><?php
                    echo $item[2] ?></td>
            </tr>
        <?php
        endforeach; ?>
    <?php
    else: ?>
        <p> Немає задач</p>
    <?php
    endif; ?>
    </tbody>
</table>

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>

</body>
<br>
<br>
<br>
<br>
<hr>
<b>Додати нову задачу</b>

<form id="task" method="POST" action="process.php">
    <div class="mb-3">
    <label for="task_name" class="form-label">Назва задачі</label>
    <input type="text" name="task_name" placeholder="Task Name" class="form-control">
    </div>
    <div class="mb-3">
    <label for="workers" class="form-label">Виконувач</label>
    </div>
    <div class="mb-3">
    <select name="workers[]" class="form-select" id="workers" multiple>
        <?php
        $workers = (new DataBase())->select('select * from workers') ?? []; ?>
        <?php
        if ( ! empty($workers)): ?>
            <?php
            foreach ($workers as $worker): ?>
                <option id="<?php
                echo $worker[0] ?>"><?php
                    echo $worker[1] ?></option>
            <?php
            endforeach; ?>
        <?php
        else: ?>
            <option>No workers available</option>
        <?php
        endif; ?>
    </select>
    </div>
    <div class="mb-3">
    <label for="task_status">Статус задачі</label>
    <br>
    <select name="task_status" id="task_status" class="form-select">
        <option id="new">New</option>
        <option id="in_progress">In progress</option>
        <option id="Done">Done</option>
    </select>
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
    <button type="reset" class="btn btn-primary">Clear</button>
</form>
<br>
<br>
<br>
<br>

<b>Додати нового працівника</b>
<br>
<form id="employ" method="POST" action="process.php">
    <div class="mb-3">
    <label for="task_name">ПІБ працівника</label>
    </div>
    <div class="mb-3">
    <input type="text" name="pib" placeholder="ПІБ">
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
    <button type="reset" class="btn btn-primary">Clear</button>
</form>

</html>
