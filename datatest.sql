SET GLOBAL sql_mode = (SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));

CREATE TABLE task_to_worker
(
    task_id   int null,
    worker_id int null
);

INSERT INTO task_to_worker (task_id, worker_id)
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (1, 4),
       (2, 1),
       (3, 2),
       (3, 3);


CREATE TABLE tasks
(
    id        int AUTO_INCREMENT
        PRIMARY KEY,
    task_name varchar(255) NOT NULL,
    status    varchar(40)  NULL
);

INSERT INTO tasks (id, task_name, status)
VALUES (1, 'Закрити проєкт', 2),
       (2, 'Написати курсову', 2),
       (3, 'Правки в В211', 2),
       (4, 'Сходити на обід', 1),
       (5, 'Виграти в лотерею', 1);

CREATE TABLE workers
(
    id  int AUTO_INCREMENT
        PRIMARY KEY,
    pib varchar(255) NULL
);

INSERT INTO workers (id, pib)
VALUES (1, 'Кермач Кремовський Васильович'),
       (2, 'Лютеран Мартіновський Розумкович'),
       (3, 'Жежвич Конецпольський Сергійович'),
       (4, 'Петро Петрович Джебович'),
       (5, 'Іванович Іван Тедович'),
       (6, 'Роман Ремович Тодорович'),
       (7, 'Павло Павлович Павленко'),
       (8, 'Павленко Іван Павлович'),
       (9, 'Павлищук Павлущенко Вікторович'),
       (10, 'Віктор Емануїл Лорантович');
