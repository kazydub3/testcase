<?php


class DataBase
{

    public function __construct()
    {
        $this->connection = $this->connection();
    }

    protected function connection()
    {
        $host = '127.0.0.1';
        $dbname = 'datatest';
        $user = 'root';
        $port = '3306';
        $pass = '12345678';


        $dbconn = new mysqli($host, $user, $pass, $dbname, $port);
        if ($dbconn->connect_error) {
            die('Error connection (' . $dbconn->connect_errno . ') '
                . $dbconn->connect_error);
        }


        return $dbconn;
    }

    public function select($query)
    {
        try {
            return $this->connection->query($query)->fetch_all();

        } catch (mysqli_sql_exception  $exception) {
            print "Error!: " . $exception->getMessage() . "<br/>";
            die();
        }
    }

    public function insertIntoTask($query, $binds = [])
    {
        try {
            $this->connection->prepare($query)->execute();
            return $this->connection->insert_id;
        } catch (mysqli_sql_exception  $exception) {
            print "Error!: " . $exception->getMessage() . "<br/>";
            die();
        }
    }

    public function insertIntoTaskToWorker($query, $binds = [])
    {
        try {
            $this->connection->prepare($query)->execute();
        } catch (mysqli_sql_exception  $exception) {
            print "Error!: " . $exception->getMessage() . "<br/>";
            die();
        }
    }

    public function getCountPerTask()
    {
        $query = 'SELECT task_name, status, COUNT(worker_id) AS task_count
FROM tasks t
         left JOIN task_to_worker ttw ON t.id = ttw.task_id
GROUP BY t.id
ORDER BY task_count DESC';

        try {
            return $this->connection->query($query)->fetch_all();
        } catch (mysqli_sql_exception  $exception) {
            print "Error!: " . $exception->getMessage() . "<br/>";
            return [];
        }
    }

    public function getCountPerWorker()
    {
        $query = 'SELECT wk.pib as pib, COUNT(task_id) AS task_count
FROM workers wk LEFT JOIN task_to_worker ttw ON wk.id = ttw.worker_id
GROUP BY wk.id';
        try {
            return $this->connection->query($query)->fetch_all();

        } catch (mysqli_sql_exception  $exception) {
            print "Error!: " . $exception->getMessage() . "<br/>";
            return [];
        }
    }

    public function insertIntoWorkers($query, $binds = [])
    {
        try {
            $this->connection->prepare($query)->execute();
            return $this->connection->insert_id;
        } catch (mysqli_sql_exception  $exception) {
            print "Error!: " . $exception->getMessage() . "<br/>";
            die();
        }
    }

}
