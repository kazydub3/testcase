<?php

include_once(__DIR__ . '/vendor/autoload.php');
include_once ("database/DataBase.php");

$request = $_SERVER['REQUEST_METHOD'];
$data    = $_POST;

if ($request === 'POST' && !empty($data)) {
    if(isset($data['pib'])){
        $data['pib'] = str_replace(
            [
                '`',
                '"',
                "'",
                "sql",
                '<?php',
                '<',
                '?',
                '>',
            ],
            '',
            strip_tags(
                trim(
                    $data['pib']
                )
            )
        );
        if(!empty($data['pib'])){
            $worker = (new DataBase())->insertIntoWorkers(
                "insert into workers set pib='{$data['pib']}'"
            );
            if($worker){
                header('Location: index.php ');
            }
        }
        header('Location: index.php?error=4');

    }

    if(empty($data['task_name'])){

        header ("Location: index.php?error=1");
        die();
    }
    if(empty($data['workers'])){

        header ("Location: index.php?error=2");
        die();
    }

    $data['task_name'] = str_replace(
        [
            '`',
            '"',
            "'",
            "sql",
            '<?php',
            '<',
            '?',
            '>',
        ],
        '',
        strip_tags(
            trim(
                $data['task_name']
            )
        )
    );


    $task_id = (new DataBase())->insertIntoTask(
        "insert into tasks set task_name='{$data['task_name']}',status='{$data['task_status']}'"
    );

    if (count($data['workers']) > 1) {
        $data['workers'] = "'" . implode("','", $data['workers']) . "'";
    } else {
        $data['workers'] = "'" . $data['workers'][0] . "'";
    }

    $workers_id = (new DataBase())->select(
        "SELECT id FROM workers WHERE
    pib IN ({$data['workers']})"
    );

    foreach ($workers_id as $item) {
        (new DataBase())->insertIntoTaskToWorker(
            "INSERT INTO task_to_worker SET task_id={$task_id}, worker_id={$item[0]}"
        );
    }

    header('Location: index.php ');

}
else {
    header ("Location: index.php?error=3");
}


